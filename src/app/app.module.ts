import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { MaterialExampleModule } from 'src/material.module';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { AppSpinnerComponent } from './components/app-spinner/app-spinner.component';
import { AppHomeComponent } from './components/app-home/app-home.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';
import { AppDialogComponent } from './components/app-dialog/app-dialog.component';
import { HomeService } from './components/app-home/service/home-service';
import { IHomeService } from './components/app-home/service/home-service.interface';

const providers = [
  { provide: IHomeService, useClass: HomeService },
];

@NgModule({
  declarations: [
    AppComponent,
    AppSpinnerComponent,
    AppHomeComponent,
    AppDialogComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    BrowserAnimationsModule,
    MaterialExampleModule,
    NgbModule,
    FormsModule, 
    ReactiveFormsModule,
    HttpClientModule
  ],
  providers: [
    ...providers
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
