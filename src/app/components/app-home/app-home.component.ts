// import { HttpClient } from '@angular/common/http';
import { HttpClient } from '@angular/common/http';
import { Component, Inject } from '@angular/core';
import { FormGroup, FormControl, Validators, FormBuilder } from '@angular/forms';
import { MatDialog, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { AppDialogComponent } from '../app-dialog/app-dialog.component';
import { IHomeService } from './service/home-service.interface';

@Component({
  selector: 'app-home',
  templateUrl: './app-home.component.html',
  styleUrls: ['./app-home.component.scss']
})
export class AppHomeComponent {

  formGroup!: FormGroup;
  myFiles: string [] = [];
  showSpinner = false;

  constructor(private fb: FormBuilder, public dialog: MatDialog, public homeService: IHomeService) {}

  ngOnInit(): void {
    this.createFormGroup();
  }

  createFormGroup(): void {
    this.formGroup = this.fb.group({
      files: new FormControl('', [Validators.required])
    });
  }
     
  onFileChange(event:any) {
    for (var i = 0; i < event.target.files.length; i++) { 
        this.myFiles.push(event.target.files[i]);
    }
  }

  async submit(): Promise<void>{
    this.showSpinner = true;
    const formData = new FormData();
 
    for (var i = 0; i < this.myFiles.length; i++) { 
      formData.append("files", this.myFiles[i]);
    }

    await this.homeService.postFiles(formData).then(
      response => {
        this.myFiles = [];
        this.formGroup.reset();
        this.showSpinner = false;
        this.openDialog('success');
      },
      error => {
        this.showSpinner = false;
        this.openDialog('error');
      }
    );
    
  }

  openDialog(type: string) {
    this.dialog.open(AppDialogComponent, {
      data: {
        type: type,
      },
    });
  }

}
