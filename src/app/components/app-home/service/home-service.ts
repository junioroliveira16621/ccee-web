import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { IHomeService } from './home-service.interface';

@Injectable({
  providedIn: 'root'
})
export class HomeService implements IHomeService {

  constructor(private http: HttpClient) { }

  async postFiles(formData: FormData): Promise<any> {
    return await this.http.post<any>('http://localhost:8080/api/v1.0/upload/files', formData).subscribe();
  }

}
